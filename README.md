## Smart City Service

Tools for classification and searching open government data

### Ubuntu

**Init**
```
apt update
apt install git -y
cd /srv/
git clone https://igreench@bitbucket.org/igreench/smart-city-deploy.git
cd /srv/smart-city-deploy/
./init.sh
```

**Upgrade**
```
cd /srv/smart-city-deploy
./upgrade.sh
```