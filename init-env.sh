#!/bin/bash

# Create app and virtual environment
git clone https://igreench@bitbucket.org/igreench/smart-city-server.git
cd /srv/scs/smart-city-server
virtualenv -p python3 venv
source venv/bin/activate
pip install -r /srv/scs/smart-city-server/requirements/prod.txt
pip install uwsgi
mkdir static
deactivate