#!/bin/bash

# for unusing sudo we you must run script with "su" command

readonly USERNAME=superuser
readonly PSWD=eWj7Uw~RcGY5%jb
readonly DEPLOY_DIRNAME=smart-city-deploy
readonly SERVICE_DIRNAME=smart-city-service
readonly SERVER_DIRNAME=smart-city-server

# if ubuntu
if [ -f /etc/lsb-release ]; then	
	# Env
	apt install nginx python3 virtualenv python3-dev build-essential -y
	mkdir /srv/scs/
	cd /srv/scs
	git clone https://igreench@bitbucket.org/igreench/smart-city-server.git
	useradd -s /bin/false -d /srv/scs scs-user
	chown -Rc scs-user:www-data /srv/scs
	chmod -R 0755 /srv/scs/
	git config --global user.email "you@example.com"
	git config --global user.name "Your Name"


	# Create app and virtual environment
	cd /srv/smart-city-deploy/
	# su -l -c "./init-env.sh" scs-user
	./init-env.sh

	# Nginx
	cp /srv/smart-city-deploy/scs.conf /etc/nginx/sites-available/scs.conf
	ln -s /etc/nginx/sites-available/scs.conf /etc/nginx/sites-enabled/
	systemctl reload nginx

	cp /srv/smart-city-deploy/scs.service /etc/systemd/system/scs.service
	systemctl start scs
	systemctl enable scs

	# curl
	apt install curl -y

	# node
	apt install nodejs -y
	apt install npm -y

	# start frontend
	cd /srv/scs/
	git clone https://igreench@bitbucket.org/igreench/smart-city-service.git
	# MY_IP=$(curl -s http://whatismyip.akamai.com/)
	# SCS_SERVER_URL=$MY_IP node app.js
	# pm2 reload ecosystem.config.js --env production # now without pm2
	cp /srv/scs/$SERVICE_DIRNAME/dist/index.html /var/www/html/
	cp -r /srv/scs/$SERVICE_DIRNAME/dist/static/ /var/www/html/

	# user
	# adduser $USERNAME --gecos "First Last,RoomNumber,WorkPhone,HomePhone" --disabled-password
	# echo "$USERNAME:$PSWD" | chpasswd
	# usermod -a -G sudo $USERNAME
	# su - $USERNAME
fi

# if centos
if [ -f /etc/redhat-release ]; then
	echo 'TODO'
fi

# node
# npm i pm2 -g # now without pm2
# curl -o- https://raw.githubusercontent.com/creationix/nvm/v0.33.11/install.sh | bash
# export NVM_DIR="$HOME/.nvm"
# [ -s "$NVM_DIR/nvm.sh" ] && \. "$NVM_DIR/nvm.sh" # This loads nvm
# nvm install 9.5.0

# start backend
# cd /srv/smart-city-server/
# pip3 install .
# python3 setup.py sdist bdist_wheel

