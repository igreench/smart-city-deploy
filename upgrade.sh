#!/bin/bash

# if ubuntu
if [ -f /etc/lsb-release ]; then
	# backend
	cd /srv/scs/smart-city-server
	git stash
	git pull origin master
	git stash apply
	virtualenv -p python3 venv
	source venv/bin/activate
	pip install -r /srv/scs/smart-city-server/requirements/prod.txt
	systemctl restart scs
	deactivate

	#frontend
	cd /srv/scs/smart-city-service
	git stash
	git pull origin master
	git stash apply
	cp /srv/scs/smart-city-service/dist/index.html /var/www/html/
	cp -r /srv/scs/smart-city-service/dist/static/ /var/www/html/
	systemctl restart nginx
fi